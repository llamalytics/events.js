// global variable to keep track of the no. of times we check for GA script
var gaLoadTries = 0;
var gaPresent = 0;	// flag of ga check
var jQueryPresent = 0;	// flag of jquery check

// function to check the presence of the GA library
function checkIfAnalyticsLoaded() {
	var d = new Date();
	var n;

	gaLoadTries++;
	
	if (typeof ga == "function") {
		// checking if GA method is available
		n = d.getTime();
		gaPresent = 1; // setting flag
	} else if (typeof _gaq.push == 'function') {
		// Do tracking with old-style analytics
		n = d.getTime();
		gaPresent = 1; // setting flag
	} else {
		if (gaLoadTries < 5) {
			// Probably want to cap the total number of times you call this.
			setTimeout(checkIfAnalyticsLoaded, 5000);		
			n = d.getTime();
			// console.log(n + ": Waiting for GA to load");
		} else {
			// perhaps GA is not there itself!
			n = d.getTime();
			console.log(n + ": GA not found. this library cannot load further!");
		}
	}
}

//loading the js library
if (window.jQuery) {  
    // jQuery is loaded  
    jQueryPresent = 1; // setting flag
    checkIfAnalyticsLoaded();
} else {
    // jQuery is not loaded
    console.log("jQuery not found. this library cannot load further!");
    // moving forward we can have a non-jquery implementation
}

// getting the jQuery object i believe
!function(e){e.fn.classes=function(t){var n=[];e.each(this,function(e,t){var r=t.className.split(/\s+/);for(var i in r){var s=r[i];if(-1===n.indexOf(s)){n.push(s)}}});if("function"===typeof t){for(var r in n){t(n[r])}}return n}}(jQuery);

jQuery( function($) {
	$(".ev-cta").click( function() {
		var action = "Click";	// for now the library supports click
		var label = "";
		var value = "";
		var category = "";
		var engagement = 0;

		var pushevent = 1;	// flag to check if event can be pushed
		// the cta class object has been clicked
		console.log("CTA Element has been clicked");

		if ( $(this).text() != "undefined" ) {
			label = $(this).text();	
		} else {
			pushevent = 0;
		}

		if ( $(this).attr("data-event-cat") != "undefined" ) {
			category = $(this).attr("data-event-cat");			
		} else {
			pushevent = 0;
		}

		if ( $(this).attr("data-value") != "undefined" ) {
			value = $(this).attr("data-value");			
		} else {
			pushevent = 0;
		}

		// category, action, label, value, non-interaction
		if ( pushevent ==1 ) {
			ga('send', 'event', category, action, label, value);
		}
	});

});