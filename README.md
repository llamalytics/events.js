# README #

Documentation for the Events.js library. 

This library is meant to be working in tandem with Google Analytics. 

### What is this repository for? ###

* To document the library and to chart out future releases
* Version: 0.01

### How do I get set up? ###

* Just include the events.js before the </body> tag. Defer the js since we dont want it to impact page loading speeds!
* The library requires [[jquery to be included]], it also [[requires GA]] account to be set.
* How to run tests - Even I have to figure this out yet!
* You will also have to go through the [[Markup Guidelines]]
* Another good place to start would be [[Known Usecases]]

### Contribution guidelines ###

* Writing tests
* Code review
* Documenting future release
* Issue tracking

### Who do I talk to? ###

* Prasad Ajinkya (@kidakaka) or Amit Sharma (@hypnosh)